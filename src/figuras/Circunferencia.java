package figuras;


/**
 * @author maria.da-costa
 * La clase Circunferencia representa una circunferencia con un radio y un color
 */
public class Circunferencia {
    private double radio;
    private String color;

    /**
     * Se crea una instancia de la clase circunferencia con el radio concreto
     *
     * @param radio el radio de la circunferencia
     */
    public Circunferencia(double radio) {
        this.radio = radio;
    }

    /**
     * Se imprime la información de la circunferencia, incluyendo el diámetro, el color y el área
     */
    public void imprimir() {

        String color = "rojo";
        System.out.println("Di�metro: " + 2 * radio);
        System.out.println("Color: " + color);
        double area = 2 * 3.1416 * radio * radio;
        System.out.println(area);
    }

    /**
     * Se compara si dos circunferencias son iguales
     *
     * @param considerarDecimales indica si se deben considerar los decimales en la comparación
     * @param otro la otra circunferencia a comparar
     * @return true si las circunferencias son iguales, o false
     */
    public boolean esIgual(boolean considerarDecimales, Circunferencia otro) {
        double radio1 = this.radio;
        double radio2 = otro.getRadio();
        if (considerarDecimales) {
            if (radio1 == radio2)
                return true;
            else
                return false;
        } else {
            if (Math.abs(radio1 - radio2) < 1)
                return true;
            else
                return false;
        }
    }
    /**
     * Se obtiene el radio de la circunferencia
     *
     * @return el radio de la circunferencia
     */
    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
}

